"""GameOfLife module"""

from copy import deepcopy


class GameOfLife:
    """GameOfLife class"""

    def __init__(self, liveCellSignature, deadCellSignature):
        self.liveCellSignature = liveCellSignature
        self.deadCellSignature = deadCellSignature


    def getNewCellValue(self, cellWithNeighbours):
        liveNeighbours = 0
        for x in range(0, 3):
            for y in range(0, 3):
                if (x == 1 and y == 1):
                    continue

                if cellWithNeighbours[x][y] == self.liveCellSignature:
                    liveNeighbours += 1

        if (cellWithNeighbours[1][1] == self.liveCellSignature): # if looking at live cell
            if (liveNeighbours < 2 or liveNeighbours > 3):
                return self.deadCellSignature
            if (liveNeighbours == 2 or liveNeighbours == 3):
                return self.liveCellSignature
        else: # else, looking at dead cell
            if (liveNeighbours == 3):
                return self.liveCellSignature

        return self.deadCellSignature


    def getNewGeneration(self, rows, columns, currentGeneration):
        expendedGeneration = deepcopy(currentGeneration)

        # expand edges with dead cells
        expendedGeneration.insert(0, [self.deadCellSignature for x in range(columns)]) # add row at the beginning
        expendedGeneration.append([self.deadCellSignature for x in range(columns)]) # add row at the end
        for row in expendedGeneration:
            row.insert(columns, self.deadCellSignature) # add dead cell at the beginning
            row.insert(0, self.deadCellSignature) # add dead sell at the end

        # create holder for new generation
        newGeneration = [[self.deadCellSignature for x in range(columns)] for y in range(rows)]

        # calculate new generation
        for r in range(1, rows + 1):
            for c in range(1, columns + 1):
                newGeneration[r-1][c-1] = self.getNewCellValue([
                    [expendedGeneration[r-1][c-1], expendedGeneration[r-1][c], expendedGeneration[r-1][c+1]],
                    [expendedGeneration[r][c-1]    , expendedGeneration[r][c]  , expendedGeneration[r][c+1]],
                    [expendedGeneration[r+1][c-1], expendedGeneration[r+1][c], expendedGeneration[r+1][c+1]]
                ])

        return newGeneration
