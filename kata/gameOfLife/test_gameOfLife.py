"""Test modul for GameOfLife class"""

import unittest

from gameOfLife import GameOfLife


class TestGetNewCellValue(unittest.TestCase):
    """Test class for GameOfLife class getNewCellValue method"""

    def setUp(self):
        self.liveCellSignature = '*'
        self.deadCellSignature = '.'
        self.gameOfLife = GameOfLife(self.liveCellSignature, self.deadCellSignature)

    def test_live_cell_with_less_than_two_live_neighbours_should_die(self):
        """Testing Scenario_1: Live cell has less than two live neighbours"""

        cellWithNeighbours = [
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
        ]

        self.assertEqual(self.deadCellSignature, self.gameOfLife.getNewCellValue(cellWithNeighbours))

    def test_live_cell_with_more_than_three_live_neighbours_should_die(self):
        """Scenario_2: Live cell has more than three live neighbours"""

        cellWithNeighbours = [
            [self.liveCellSignature, self.liveCellSignature, self.liveCellSignature],
            [self.liveCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
        ]

        self.assertEqual(self.deadCellSignature, self.gameOfLife.getNewCellValue(cellWithNeighbours))

    def test_live_cell_with_two_live_neighbours_should_live(self):
        """Scenario_3: Live cell has two live neighbours"""

        cellWithNeighbours = [
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.liveCellSignature, self.liveCellSignature],
            [self.liveCellSignature, self.deadCellSignature, self.deadCellSignature],
        ]

        self.assertEqual(self.liveCellSignature, self.gameOfLife.getNewCellValue(cellWithNeighbours))

    def test_live_cell_with_three_live_neighbours_should_live(self):
        """Scenario_4: Live cell has three live neighbours"""

        cellWithNeighbours = [
            [self.liveCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.liveCellSignature],
        ]

        self.assertEqual(self.liveCellSignature, self.gameOfLife.getNewCellValue(cellWithNeighbours))

    def test_dead_cell_with_three_live_neighbours_should_live(self):
        """Scenario_5: Dead cell has three live neighbours"""

        cellWithNeighbours = [
            [self.liveCellSignature, self.deadCellSignature, self.liveCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
        ]

        self.assertEqual(self.liveCellSignature, self.gameOfLife.getNewCellValue(cellWithNeighbours))



class TestGetNewGeneration(unittest.TestCase):
    """Test class for GameOfLife class getNewGeneration method"""

    def setUp(self):
        self.liveCellSignature = '*'
        self.deadCellSignature = '.'
        self.gameOfLife = GameOfLife(self.liveCellSignature, self.deadCellSignature)

    def test_live_cells_with_less_than_two_live_neighbours_should_die(self):
        """Testing Scenario_1: Live cell has less than two live neighbours"""

        rows, columns = 3, 4

        currentGeneration = [
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.liveCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
        ]

        newGeneration = self.gameOfLife.getNewGeneration(rows, columns, currentGeneration)

        self.assertEqual(self.deadCellSignature, newGeneration[1][1])
        self.assertEqual(self.deadCellSignature, newGeneration[2][2])

    def test_live_cells_with_more_than_three_live_neighbours_should_die(self):
        """Scenario_2: Live cell has more than three live neighbours"""

        rows, columns = 4, 3

        currentGeneration = [
            [self.liveCellSignature, self.liveCellSignature, self.liveCellSignature],
            [self.liveCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
        ]

        newGeneration = self.gameOfLife.getNewGeneration(rows, columns, currentGeneration)

        self.assertEqual(self.deadCellSignature, newGeneration[0][1])
        self.assertEqual(self.deadCellSignature, newGeneration[1][1])

    def test_live_cells_with_two_live_neighbours_should_live(self):
        """Scenario_3: Live cell has two live neighbours"""

        rows, columns = 4, 4

        currentGeneration = [
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.liveCellSignature, self.liveCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.liveCellSignature, self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.liveCellSignature, self.deadCellSignature, self.liveCellSignature, self.liveCellSignature],
        ]

        newGeneration = self.gameOfLife.getNewGeneration(rows, columns, currentGeneration)

        self.assertEqual(self.liveCellSignature, newGeneration[1][0])
        self.assertEqual(self.liveCellSignature, newGeneration[1][2])
        self.assertEqual(self.liveCellSignature, newGeneration[3][3])

    def test_live_cells_with_three_live_neighbours_should_live(self):
        """Scenario_4: Live cell has three live neighbours"""

        rows, columns = 5, 4

        currentGeneration = [
            [self.deadCellSignature, self.deadCellSignature, self.liveCellSignature, self.liveCellSignature],
            [self.liveCellSignature, self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.liveCellSignature, self.liveCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.liveCellSignature, self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.liveCellSignature, self.liveCellSignature],
        ]

        newGeneration = self.gameOfLife.getNewGeneration(rows, columns, currentGeneration)

        self.assertEqual(self.liveCellSignature, newGeneration[1][2])
        self.assertEqual(self.liveCellSignature, newGeneration[2][0])
        self.assertEqual(self.liveCellSignature, newGeneration[3][2])
    
    def test_dead_cells_with_three_live_neighbours_should_live(self):
        """Scenario_5: Dead cell has three live neighbours"""

        rows, columns = 6, 4

        currentGeneration = [
            [self.liveCellSignature, self.deadCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.liveCellSignature, self.liveCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
            [self.liveCellSignature, self.liveCellSignature, self.liveCellSignature, self.deadCellSignature],
            [self.deadCellSignature, self.deadCellSignature, self.deadCellSignature, self.deadCellSignature],
        ]

        newGeneration = self.gameOfLife.getNewGeneration(rows, columns, currentGeneration)

        self.assertEqual(self.liveCellSignature, newGeneration[1][1])
        self.assertEqual(self.liveCellSignature, newGeneration[3][3])
        self.assertEqual(self.liveCellSignature, newGeneration[5][1])


unittest.main()
