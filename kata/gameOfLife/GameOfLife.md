# &#9745; Feature: Game Of Life

*Description*: <br>
Your task is to write a program to calculate the next generation of Conway's game of life, given any starting position. <br>
You start with a two dimensional grid of cells, where each cell is either alive or dead. The grid is finite, and no life can exist off the edges. Rules are described as scenarios.

**As** a player <br>
**I want** to get next generation of *Conway's Game Of Life* <br>
**So that** I can play the game <br>

## &#9745; Scenario_1: Live cell has less than two live neighbours
**Given**: Grid dimensions <br>
**And**: cell values <br>
**When**: user requests new generation <br>
**And**: live cell contains fewer than two live neighbours <br>
**Then**: that cell is dead in the new generation <br>

## &#9745; Scenario_2: Live cell has more than three live neighbours
**Given**: Grid dimensions <br>
**And**: cell values <br>
**When**: user requests new generation <br>
**And**: live cell contains more than three live neighbours <br>
**Then** that cell is dead in the new generation <br>

## &#9745; Scenario_3: Live cell has two live neighbours
**Given**: Grid dimensions <br>
**And**: cell values <br>
**When**: user requests new generation <br>
**And**: live cell contains two live neighbours <br>
**Then**: that cell is live in the new generation <br>

## &#9745; Scenario_4: Live cell has three live neighbours
**Given**: Grid dimensions <br>
**And**: cell values <br>
**When**: user requests new generation <br>
**And**: live cell contains three live neighbours <br>
**Then**: that cell is live in the new generation <br>

## &#9745; Scenario_5: Dead cell has three live neighbours
**Given**: Grid dimensions <br>
**And**: cell values <br>
**When**: user requests new generation <br>
**And**: dead cell contains three live neighbours <br>
**Then**: that cell is live in the new generation <br>
